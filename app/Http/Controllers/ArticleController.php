<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Goutte\Client;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->get();
        return view('home')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
      $this -> validate($request,[
        'link' => 'required',
      ]);

      $client = new Client();
      $crawler = $client->request('GET', $request->input('link'));

      $title = $crawler->filter('title')->each(function ($node) {
            $text = "";
            $text .= $node->text();
            return $text;
      });

      $body = $crawler->filter('p')->each(function ($node) {
            $text = "";

            $text .= $node->text() . "\n";
            return $text;
      });


      $article = new article;
      $article->link = $request->input('link');
      $article->title = implode(' ',$title);
      $article->content = implode(' ', $body);

      //Save message
      $article->save();

      return redirect('/')->with('success', 'article added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('show')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

        return redirect('/')->with('success', 'Article removed!');
    }
}

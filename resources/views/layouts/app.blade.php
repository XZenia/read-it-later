<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Read It Later V0.1</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/custom.css">
  </head>
  <body>
    @include('inc.navbar')
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-lg-8">
          @include('inc/error-popup')
          @yield('content')
        </div>
        <div class="col-md-4 col-lg-4">
          @include('inc.sidebar')
        </div>
      </div>
    </div>
    @include('inc.footer')
  </body>
</html>

@extends('layouts.app')
@section('content')
<h1> Saved Articles </h1>
  @if (count($articles) > 0)
    @foreach($articles as $article)
      <div class="well well-sm">
          <a href="article/{{$article->id}}"> <h2>{{$article->title}}</h2> </a>
          <h4 class="label label-danger"> {{$article->link}} </h4>
      </div>
    @endforeach
  @endif
@endsection

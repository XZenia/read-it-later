@extends('layouts.app')

@section('content')
<h1> Add a new article </h1>

{!! Form::open(['url' => 'article/submit']) !!}
  <div class="form-group">
    {{ Form::label ('link-label', 'Article Link: ') }}
    {{ Form::text ('link', '', ['class' => 'form-control', 'placeholder' => 'https://medium.com/mindorks/how-to-write-clean-code-lessons-learnt-from-the-clean-code-robert-c-martin-9ffc7aef870c']) }}
  </div>

  <div class="col text-right">
    {{ Form::submit('Submit'), ['class' => 'btn btn-primary'] }}
  </div>
{!! Form::close() !!}

@endsection
